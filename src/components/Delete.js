import axios from "axios";
import { AiOutlineDelete } from "react-icons/ai";
import { useDispatch } from "react-redux";
import { deletePictures } from "../feature/pictures.slice";
import { url } from "../ServerUrl";

const Delete = ({ id }) => {
  const dispatch = useDispatch();
  const handleDelete = () => {
    axios.delete(url + id);
    dispatch(deletePictures(id));
    console.log("L'ID du poste supprimé : " + id);
  };
  return (
    <div className="delete-icon" onClick={() => handleDelete()}>
      <AiOutlineDelete />
    </div>
  );
};

export default Delete;
