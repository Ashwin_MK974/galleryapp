import { createSlice } from "@reduxjs/toolkit";

const pituresSlices = createSlice({
  name: "pictures",
  initialState: {
    pictures: null,
  },
  reducers: {
    setPicturesData: (state, action) => {
      state.pictures = action.payload;
    },
    addPictures: (state, { payload }) => {
      //state.pictures = [...state.pictures, payload];
      state.pictures.push(payload);
    },
    editPictures: (state, { payload }) => {
      state.pictures = state.pictures.map((picture) => {
        if (picture.id == payload[0]) {
          return { ...picture, artist: payload[1] };
        } else {
          return picture;
        }
      });
    },
    deletePictures: (state, { payload }) => {
      state.pictures = state.pictures.filter(
        (picture) => picture.id != payload
      );
    },
  },
});
export const { setPicturesData, addPictures, editPictures, deletePictures } =
  pituresSlices.actions;
export default pituresSlices.reducer;
